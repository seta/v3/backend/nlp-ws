import os

# Gunicorn config variables
worker_class = "uvicorn.workers.UvicornWorker"
loglevel = os.environ.get("LOG_LEVEL", default="WARNING")
errorlog = "-"  # stderr
accesslog = "-"  # stdout
name = "seta-nlp"
worker_tmp_dir = "/dev/shm"
timeout = 600
workers = 2
