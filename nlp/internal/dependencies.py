import os

from nlp import configuration
from nlp.internal.implementations import (
    tika_parser,
    sentence_transformer_embeddings as ste,
)


def get_file_parser():
    """Creates a Tika parser client."""

    #! see ENV variable in Dockerfile
    tika_path = os.environ["TIKA_PATH"]

    return tika_parser.TikaParser(tika_path=tika_path)


def get_embeddings_client():
    """Creates embeddings client using SetaTransformer"""

    return ste.SentenceTransformerEmbeddings(
        use_workers=configuration.app_config.USE_EMBEDDINGS_WORKER,
        disable_embeddings=configuration.app_config.DISABLE_COMPUTE_EMBEDDINGS,
    )
