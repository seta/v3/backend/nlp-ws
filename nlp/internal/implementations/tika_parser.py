import asyncio
import logging
from interface import implements

from nlp.internal.interfaces import file_parser
from nlp.internal import exceptions

logger = logging.getLogger("nlp")


class TikaParser(implements(file_parser.IFileParserAsync)):
    def __init__(self, tika_path: str):
        self.tika_path = tika_path

    async def extract_text(self, content: bytes, output_format: str = "text") -> str:
        """Extracts text from file content."""

        proc = await asyncio.create_subprocess_shell(
            f"java -jar {self.tika_path}  --{output_format} -r",
            stdout=asyncio.subprocess.PIPE,
            stdin=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )

        stdout, stderr = await proc.communicate(input=content)

        if stdout:
            return stdout.decode(encoding="utf-8")

        if stderr:
            msg = stderr.decode(encoding="utf-8")
            raise exceptions.TikaParserException(msg)

        return None
