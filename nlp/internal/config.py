import configparser
import os


class Config:
    """Application configuration"""

    SECRET_KEY = None
    DISABLE_COMPUTE_EMBEDDINGS = False

    def __init__(self, section_name: str, config_file: str) -> None:
        config = configparser.ConfigParser()
        config.read(config_file)

        sections = config.sections()
        if len(sections) == 0:
            message = (
                f"No configuration section found in the config file ('{config_file}')"
            )
            raise Exception(message)  # pylint: disable=broad-exception-raised

        if section_name not in sections:
            # pylint: disable-next=broad-exception-raised
            raise Exception("section_name parameter must be one of " + str(sections))

        config_section = config[section_name]

        Config._init_env_variables()
        Config._init_config_section(config_section)

    @staticmethod
    def get_env_boolean(name: str, default_value: bool | None = None) -> bool:
        """Get a boolean value from an environment variable."""

        true_ = (
            "true",
            "1",
            "t",
            "yes",
        )  # Add more entries if you want, like: `y`, `yes`, `on`, ...
        false_ = (
            "false",
            "0",
            "f",
            "no",
        )  # Add more entries if you want, like: `n`, `no`, `off`, ...

        value: str | None = os.getenv(key=name, default=None)
        if value is None:
            if default_value is None:
                raise ValueError(f"Variable `{name}` not set!")

            value = str(default_value)

        if value.lower() not in true_ + false_:
            raise ValueError(f"Invalid value `{value}` for variable `{name}`")
        return value in true_

    @staticmethod
    def _init_env_variables():
        # ===== Read environment variables ======#

        # flask
        Config.SECRET_KEY = os.environ.get("API_SECRET_KEY")

        # nlp
        Config.DISABLE_COMPUTE_EMBEDDINGS = Config.get_env_boolean(
            name="DISABLE_COMPUTE_EMBEDDINGS", default_value=False
        )

    @staticmethod
    def _init_config_section(config_section: configparser.SectionProxy):
        """Check the seta_config/nlp.conf file for documentation."""

        Config.TESTING = config_section.getboolean("TESTING", fallback=False)
        Config.USE_EMBEDDINGS_WORKER = Config.get_env_boolean(
            name="USE_EMBEDDINGS_WORKER",
            default_value=config_section.getboolean(
                "USE_EMBEDDINGS_WORKER", fallback=False
            ),
        )
