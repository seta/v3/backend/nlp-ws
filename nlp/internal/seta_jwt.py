from typing import Any, Dict, Optional
import logging

from fastapi_jwt import JwtAccessBearerCookie, JwtAuthorizationCredentials


logger = logging.getLogger("nlp")


class JwtDecodedToken(dict):
    def __init__(self, payload: Dict[str, Any]):
        super().__init__(payload)


class SetaJwtAccessBearerCookie(JwtAccessBearerCookie):
    def __init__(self, secret_key: str):
        super().__init__(secret_key=secret_key)

    async def _get_credentials(
        self, bearer, cookie
    ) -> Optional[JwtAuthorizationCredentials]:
        payload = await self._get_payload(bearer, cookie)

        if payload:
            return JwtDecodedToken(payload)

        return None
