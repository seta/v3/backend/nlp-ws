from interface import Interface


class IEmbeddingsAsync(Interface):

    async def chunks_and_embeddings_from_text(self, text: str):
        """Compute embeddings from text"""

        pass

    async def embedding_vector_from_text(self, text: str):
        """Compute vector from text"""

        pass
