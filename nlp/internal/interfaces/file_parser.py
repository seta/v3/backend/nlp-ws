from interface import Interface


class IFileParserAsync(Interface):
    async def extract_text(self, content: bytes, output_format: str = "text") -> str:
        """Extracts text from file content."""

        pass
