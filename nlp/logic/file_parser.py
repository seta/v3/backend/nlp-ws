from fastapi import status

from nlp import globals as g
from nlp.internal import file_validation, exceptions


async def extract_text_from_file(accept: str, file_content: bytes) -> tuple[str, str]:
    """Extracts text from file content."""

    is_allowed, mime = file_validation.allowed_extension(file_content)

    if not is_allowed:
        raise exceptions.NLPException(
            status_code=status.HTTP_403_FORBIDDEN,
            message=f"Detected file extension not allowed: {mime}",
        )

    match accept:
        case "text/html":
            extract_format = "html"
        case "text/xml":
            extract_format = "xml"
        case _:
            extract_format = "text"

    text = await g.file_parser.extract_text(
        content=file_content, output_format=extract_format
    )
    return text, mime
