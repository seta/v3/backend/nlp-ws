import logging

from fastapi import APIRouter, HTTPException, Security, status

from nlp import models
from nlp import globals as g
from nlp.internal import seta_jwt

from nlp.access_security import access_security

router = APIRouter(tags=["Embeddings"])
internal_router = APIRouter(tags=["Internal"])

logger = logging.getLogger("nlp")


@router.post(
    "/compute_embeddings",
    summary="Embeddings from plain text.",
    description="Given a plain text, related embeddings are provided using Doc2vec.",
)
async def embeddings_from_text(
    text: models.ParserText,
    decoded_token: seta_jwt.JwtDecodedToken = Security(access_security),
) -> models.Chunks:
    """Extracts embeddings from text."""

    # auto_error=False, fo we should check manually
    if not decoded_token:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Unauthorized"
        )

    try:
        return await g.embeddings_transformer.chunks_and_embeddings_from_text(
            text=text.text
        )
    except Exception as e:
        logger.exception(e)

        # pylint: disable-next=raise-missing-from
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Internal server error.",
        )


@router.post(
    "/compute_embedding",
    summary="Vector from text",
    description="Given a plain text, related embeddings vector is provided.",
)
async def vector_from_text(
    text: models.ParserText,
    decoded_token: seta_jwt.JwtDecodedToken = Security(access_security),
) -> models.Vector:
    """Extracts embedding vector from text."""

    # auto_error=False, fo we should check manually
    if not decoded_token:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Unauthorized"
        )

    try:
        return await g.embeddings_transformer.embedding_vector_from_text(text.text)
    except Exception as e:
        logger.exception(e)

        # pylint: disable-next=raise-missing-from
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Internal server error.",
        )


@internal_router.post(
    "/compute_embeddings",
    summary="Embeddings from plain text.",
    description="Given a plain text, related embeddings are provided using Doc2vec.",
)
async def internal_embeddings_from_text(
    text: models.ParserText,
) -> models.Chunks:
    """Extracts embeddings from text."""

    try:
        return await g.embeddings_transformer.chunks_and_embeddings_from_text(
            text=text.text
        )
    except Exception as e:
        logger.exception(e)

        # pylint: disable-next=raise-missing-from
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Internal server error.",
        )


@internal_router.post(
    "/compute_embedding",
    summary="Vector from text",
    description="Given a plain text, related embeddings vector is provided.",
)
async def internal_vector_from_text(
    text: models.ParserText,
) -> models.Vector:
    """Extracts embedding vector from text."""

    try:
        return await g.embeddings_transformer.embedding_vector_from_text(text.text)
    except Exception as e:
        logger.exception(e)

        # pylint: disable-next=raise-missing-from
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Internal server error.",
        )
