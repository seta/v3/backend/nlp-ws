import logging
from typing import Annotated
from fastapi import APIRouter, HTTPException, Security, UploadFile, status, Header

from fastapi.responses import PlainTextResponse

from nlp.internal import seta_jwt, exceptions
from nlp.logic import file_parser as file_parser_logic

from nlp.access_security import access_security


router = APIRouter(tags=["File Parser"])
internal_router = APIRouter(tags=["Internal"])

logger = logging.getLogger("nlp")


@router.post(
    "/file_to_text",
    response_class=PlainTextResponse,
    summary="Extract text",
    description="Extracts text from uploaded file. \
                Specify the `Accept` header to get the response in the desired format: \
                **text/plain (default)**, **text/html**, **text/xml**.",
)
async def parse_text(
    file: UploadFile,
    accept: Annotated[
        str | None, Header(examples=["text/plain", "text/html", "text/xml"])
    ] = "text/plain",
    decoded_token: seta_jwt.JwtDecodedToken = Security(access_security),
):
    """Extracts text from uploaded file."""

    try:
        # auto_error=False, fo we should check manually
        if not decoded_token:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail="Unauthorized"
            )

        file_content = await file.read()

        text, mime = await file_parser_logic.extract_text_from_file(
            accept, file_content
        )

        response = PlainTextResponse(content=text)
        response.headers["X-File-Extension"] = mime

        return response
    except HTTPException:
        raise
    except exceptions.NLPException as e:
        logger.exception(e)

        raise HTTPException(
            status_code=e.status_code,
            detail=e.message,
        ) from e
    except Exception as e:
        logger.exception(e)

        # pylint: disable-next=raise-missing-from
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred during extracting text from file.",
        )
    finally:
        await file.close()


@internal_router.post(
    "/file_to_text",
    response_class=PlainTextResponse,
    summary="Extract text",
    description="Extracts text from uploaded file. \
                Specify the `Accept` header to get the response in the desired format: \
                **text/plain (default)**, **text/html**, **text/xml**.",
)
async def internal_parse_text(
    file: UploadFile, accept: Annotated[str | None, Header()] = None
):
    """Extracts text from uploaded file."""

    try:
        file_content = await file.read()

        text, mime = await file_parser_logic.extract_text_from_file(
            accept, file_content
        )

        response = PlainTextResponse(content=text)
        response.headers["X-File-Extension"] = mime

        return response
    except HTTPException:
        raise
    except exceptions.NLPException as e:
        logger.exception(e)

        raise HTTPException(
            status_code=e.status_code,
            detail=e.message,
        ) from e
    except Exception as e:
        logger.exception(e)

        # pylint: disable-next=raise-missing-from
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred during extracting text from file.",
        )
    finally:
        await file.close()
