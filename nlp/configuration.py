import os
from nlp.internal import config

STAGE = "Development"
CONFIG_APP_FILE = "/etc/seta/nlp.conf"

app_config: config.Config = None


def init():
    """Initialize configuration"""

    global app_config, STAGE  # pylint: disable=global-statement

    STAGE = os.environ.get("STAGE", default="Development")
    app_config = config.Config(section_name=STAGE, config_file=CONFIG_APP_FILE)

    # huggingface/tokenizers: disable parallelism to avoid deadlocks...
    os.environ["TOKENIZERS_PARALLELISM"] = "false"
