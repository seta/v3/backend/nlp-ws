from nlp.internal.seta_jwt import SetaJwtAccessBearerCookie
from nlp import configuration

# Read access token from bearer header and cookie (bearer priority)
access_security = SetaJwtAccessBearerCookie(
    secret_key=configuration.app_config.SECRET_KEY,
)
