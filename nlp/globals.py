from nlp.internal.interfaces import file_parser as ifp, embeddings as ie
from nlp.internal import dependencies

file_parser: ifp.IFileParserAsync = None
embeddings_transformer: ie.IEmbeddingsAsync = None


def init():
    """Initialize globals."""

    # pylint: disable-next=global-statement
    global file_parser, embeddings_transformer

    file_parser = dependencies.get_file_parser()
    embeddings_transformer = dependencies.get_embeddings_client()


def close():
    """Clear resources."""

    # pylint: disable-next=global-statement
    global file_parser, embeddings_transformer

    file_parser = None
    embeddings_transformer = None
