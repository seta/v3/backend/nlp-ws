import time
import logging
import random
import string

from collections.abc import AsyncIterator
from contextlib import asynccontextmanager

from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware

from nlp import configuration, globals as g

logger = logging.getLogger("nlp")


@asynccontextmanager
async def lifespan(_: FastAPI) -> AsyncIterator[None]:
    """Lifespan context manager."""

    g.init()

    logger.info("NLP app lifespan initialized.")

    yield

    g.close()


def create_fastapi_app() -> FastAPI:
    """Web service app factory"""

    app = FastAPI(
        root_path="/seta-nlp",
        title="SeTA NLP",
        summary="Natural language processing web service.",
        version="1.0.0",
        lifespan=lifespan,
    )

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    @app.middleware("http")
    async def log_requests(request: Request, call_next):
        """Logs the time every request takes."""

        if configuration.STAGE.lower() == "test":
            return await call_next(request)

        idem = "".join(random.choices(string.ascii_uppercase + string.digits, k=6))
        logger.debug("rid=%s start request path=%s", idem, request.url.path)
        start_time = time.time()

        response = await call_next(request)

        process_time = (time.time() - start_time) * 1000
        formatted_process_time = f"{process_time:.2f}"
        logger.debug(
            "rid=%s completed_in=%sms status_code=%s",
            idem,
            formatted_process_time,
            response.status_code,
        )

        return response

    _register_routers(app)

    return app


def _register_routers(app: FastAPI):
    # pylint: disable=import-outside-toplevel
    from nlp.routers.file_parser import (
        router as file_parser_router,
        internal_router as internal_file_parser_router,
    )
    from nlp.routers.embeddings import (
        router as embeddings_router,
        internal_router as internal_embeddings_router,
    )
    from nlp.routers.concordance import router as concordance_router

    app.include_router(file_parser_router)
    app.include_router(embeddings_router)
    app.include_router(
        concordance_router,
        prefix="/internal",
        include_in_schema=configuration.STAGE.lower() == "development",
    )
    app.include_router(
        internal_embeddings_router,
        prefix="/internal",
        include_in_schema=configuration.STAGE.lower() == "development",
    )
    app.include_router(
        internal_file_parser_router,
        prefix="/internal",
        include_in_schema=configuration.STAGE.lower() == "development",
    )
