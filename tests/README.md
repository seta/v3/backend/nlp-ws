## NLP Testing

NLP web service is using pytest for functional and unit testing.


### Configuration

Create a file with name *secrets.conf* for [configparser](https://docs.python.org/3.10/library/configparser.html) that looks like this:

```
[TEST]
# same secret as in seta-compose/.env.test
API_SECRET_KEY= 
```

The configuration file is read by ConfigParser in *conftest.py*, *init_os* fixture.

Note: This file name is set to be ignored by git in *.gitignore*.