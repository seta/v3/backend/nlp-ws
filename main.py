import logging
import logging.config
import os

from fastapi import FastAPI

from nlp import configuration
from nlp.factory import create_fastapi_app


# setup loggers
logging.config.fileConfig("/etc/seta/logging.conf", disable_existing_loggers=False)
log_level = os.environ.get("LOG_LEVEL", default=logging.getLogger().getEffectiveLevel())

root_logger = logging.getLogger()
root_logger.setLevel(log_level)

nlp_logger = logging.getLogger("nlp")
nlp_logger.setLevel(log_level)


def create_app() -> FastAPI:
    """Web service app factory"""

    # create configuration
    configuration.init()

    app = create_fastapi_app()

    root_logger.info("FastAPI NLP initialized.")
    print("Log level: " + str(log_level))

    return app
