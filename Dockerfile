FROM python:3.10

ARG HTTPS_PROXY
ARG HTTP_PROXY

ARG https_proxy=$HTTPS_PROXY
ARG http_proxy=$HTTP_PROXY

RUN echo "Acquire::http::Proxy \""$HTTP_PROXY"\";" >> /etc/apt/apt.conf \
    && echo "Acquire::https::Proxy \""$HTTPS_PROXY"\";" >> /etc/apt/apt.conf \
    && export https_proxy=$HTTPS_PROXY \
    && export http_proxy=$HTTP_PROXY

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV TIKA_PATH="tika/tika-app-2.4.1.jar"

RUN apt update -y   \
    && apt install -y --no-install-recommends \
    wget \
    default-jre \
    libmagic1 \
    && apt install -y gpg \
    && rm -f /etc/apt/apt.conf

RUN useradd seta
ARG ROOT=/home/seta

RUN mkdir -p $ROOT/tika
WORKDIR $ROOT/tika

RUN wget https://archive.apache.org/dist/tika/2.4.1/tika-app-2.4.1.jar \
    && wget https://archive.apache.org/dist/tika/2.4.1/tika-app-2.4.1.jar.asc \
    && wget https://archive.apache.org/dist/tika/KEYS \
    && gpg --import KEYS \
    && gpg --verify tika-app-2.4.1.jar.asc tika-app-2.4.1.jar

WORKDIR $ROOT

COPY ./requirements_model.txt .
RUN pip install --no-cache-dir -r requirements_model.txt

RUN python3 -m nltk.downloader punkt \
    && python3 -c 'import nltk; nltk.download("punkt_tab")' \
    && python3 -c 'from sentence_transformers import SentenceTransformer; SentenceTransformer("intfloat/multilingual-e5-large-instruct")'

COPY ./requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

#copy configuration files
COPY ./config/logging.conf /etc/seta/
COPY ./config/nlp.conf /etc/seta/

COPY ./nlp ./nlp
COPY ./*.py .

CMD ["gunicorn", "--conf", "/home/seta/gunicorn_conf.py", "main:create_app()", "--bind=0.0.0.0:8000"]